#!/usr/bin/env sh

set -ex

GIT_TAG='4ca828a953a18b7985c6c1c6b850740a17dd73d1'
TAG=${1-$GIT_TAG}
DOCKER_IMAGE=${DOCKER_IMAGE-''}
if [ ! -z "${DOCKER_IMAGE}" ]
then
    DOCKER_IMAGE=${DOCKER_IMAGE}"/"
    # try to pull previously built images to utilize docker cache during new build
    docker pull ${DOCKER_IMAGE}composer:latest || true
    docker pull ${DOCKER_IMAGE}node-admin:latest || true
    docker pull ${DOCKER_IMAGE}node-website:latest || true
    docker pull ${DOCKER_IMAGE}apache:latest || true
fi;


# build and tag intermediate images and application image
docker build . --target composer -t ${DOCKER_IMAGE}composer:latest \
            --cache-from ${DOCKER_IMAGE}composer:latest; 

docker build . --target node-admin -t ${DOCKER_IMAGE}node-admin:latest \
            --cache-from ${DOCKER_IMAGE}composer:latest \
            --cache-from ${DOCKER_IMAGE}node-admin:latest

docker build . --target node-website -t ${DOCKER_IMAGE}node-website:latest \
            --cache-from ${DOCKER_IMAGE}composer:latest \ 
            --cache-from ${DOCKER_IMAGE}node-admin:latest \
            --cache-from ${DOCKER_IMAGE}node-website:latest

docker build . --target apache -t ${DOCKER_IMAGE}apache:latest \
            --cache-from ${DOCKER_IMAGE}composer:latest \
            --cache-from ${DOCKER_IMAGE}node-admin:latest \
            --cache-from ${DOCKER_IMAGE}node-website:latest \
            --cache-from ${DOCKER_IMAGE}apache:latest

docker build . --target production -t ${DOCKER_IMAGE}production:$TAG \
            --cache-from ${DOCKER_IMAGE}composer:latest \
            --cache-from ${DOCKER_IMAGE}node-admin:latest \
            --cache-from ${DOCKER_IMAGE}node-website:latest \
            --cache-from ${DOCKER_IMAGE}apache:latest

docker build . --target staging  -t ${DOCKER_IMAGE}staging:$TAG \
            --cache-from ${DOCKER_IMAGE}composer:latest \
            --cache-from ${DOCKER_IMAGE}node-admin:latest \
            --cache-from ${DOCKER_IMAGE}node-website:latest \
            --cache-from ${DOCKER_IMAGE}apache:latest

docker build . --target development  -t ${DOCKER_IMAGE}development:$TAG \
            --cache-from ${DOCKER_IMAGE}composer:latest \
            --cache-from ${DOCKER_IMAGE}node-admin:latest \
            --cache-from ${DOCKER_IMAGE}node-website:latest \
            --cache-from ${DOCKER_IMAGE}apache:latest \
if [ ! -z "${DOCKER_IMAGE}" ]
then
# PUSH INTO REGISTRY
    docker push ${DOCKER_IMAGE}composer:latest
    docker push ${DOCKER_IMAGE}node-admin:latest
    docker push ${DOCKER_IMAGE}node-website:latest
    docker push ${DOCKER_IMAGE}server:latest
    docker push ${DOCKER_IMAGE}production:$TAG
    docker push ${DOCKER_IMAGE}staging:$TAG
    docker push ${DOCKER_IMAGE}development:$TAG
fi;