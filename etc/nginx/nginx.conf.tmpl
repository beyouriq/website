pid /var/run/nginx.pid;

user {{ .Env.SYSTEM_APPUSER_NAME }};
error_log /dev/stdout notice;

worker_processes auto;
events {
  multi_accept on;
  use epoll;
  worker_connections 1024;
}

http {
    log_format json_combined escape=json
      '{'
        '"time_local":"$time_local",'
        '"remote_addr":"$remote_addr",'
        '"remote_user":"$remote_user",'
        '"request":"$request",'
        '"status": "$status",'
        '"body_bytes_sent":"$body_bytes_sent",'
        '"request_time":"$request_time",'
        '"http_referrer":"$http_referer",'
        '"http_user_agent":"$http_user_agent"'
      '}';
    server {
        error_log /dev/stdout notice;
        access_log /dev/stdout json_combined;
        server_name   ~^(www\.)(?<domain>.+)$;
        expires       max;
        return        301 http://$domain$request_uri;
    }

    server {
        listen 80 default_server;
        index index.php index.html;
        root {{ .Env.APP_WEBROOT }};

        error_log /dev/stdout notice;
        access_log /dev/stdout json_combined;

        real_ip_header X-Forwarded-For;
        # Ignore trusted IPs
        real_ip_recursive on;
        set $context {{ .Env.APP_ENV }};

        location / {
            # try to serve file directly, fallback to index.php
            try_files $uri /index.php$is_args$args;
        }

        # expire
        location ~* \.(?:ico|css|js|gif|jpe?g|png|svg|woff|woff2|eot|ttf)$ {
            # try to serve file directly, fallback to index.php
            try_files $uri /index.php$is_args$args;
            access_log off;
            expires 30d;
            add_header Pragma public;
            add_header Cache-Control "public";
        }

        # pass the PHP scripts to FastCGI server from upstream phpfcgi
        location ~ ^/(index|config)\.php(/|$) {
            fastcgi_pass unix:/var/run/php-fpm-www.sock;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
            fastcgi_param FLOW_CONTEXT $context;
            fastcgi_param FLOW_REWRITEURLS 1;
            internal;
        }
    }

    # Root definitions
    include     /etc/nginx/nginx.d/*.conf;
    include     /etc/nginx/hosts.d/*.conf;

}
