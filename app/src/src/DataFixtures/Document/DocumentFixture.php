<?php

namespace App\DataFixtures\Document;

use App\DataFixtures\ORM\AppFixtures;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use RuntimeException;
use Sulu\Bundle\DocumentManagerBundle\DataFixtures\DocumentFixtureInterface;
use Sulu\Bundle\MediaBundle\Entity\Media;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Bundle\SnippetBundle\Document\SnippetDocument;
use Sulu\Bundle\SnippetBundle\Snippet\DefaultSnippetManagerInterface;
use Sulu\Component\Content\Document\RedirectType;
use Sulu\Component\Content\Document\WorkflowStage;
use Sulu\Component\DocumentManager\DocumentManager;
use Sulu\Component\DocumentManager\Exception\DocumentManagerException;
use Sulu\Component\DocumentManager\Exception\MetadataNotFoundException;
use Sulu\Component\PHPCR\PathCleanup;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DocumentFixture implements DocumentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @var PathCleanup
     */
    private $pathCleanup;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function getOrder(): int
    {
        return 10;
    }

    /**
     * @param mixed[] $data
     *
     * @throws MetadataNotFoundException
     */
    private function createPage(DocumentManager $documentManager, array $data): BasePageDocument
    {
        $locale = isset($data['locale']) && $data['locale'] ? $data['locale'] : AppFixtures::LOCALE_DE;

        if (!isset($data['url'])) {
            $url = $this->getPathCleanup()->cleanup('/' . $data['title']);
            if (isset($data['parent_path'])) {
                $url = mb_substr($data['parent_path'], mb_strlen('/cmf/beyouriq/contents')) . $url;
            }

            $data['url'] = $url;
        }

        $extensionData = [
            'seo' => $data['seo'] ?? [],
            'excerpt' => $data['excerpt'] ?? [],
        ];

        unset($data['excerpt']);
        unset($data['seo']);

        /** @var PageDocument $pageDocument */
        $pageDocument = null;

        try {
            if (!isset($data['id']) || !$data['id']) {
                throw new Exception();
            }

            $pageDocument = $documentManager->find($data['id'], $locale);
        } catch (Exception $e) {
            $pageDocument = $documentManager->create('page');
        }

        $pageDocument->setNavigationContexts($data['navigationContexts'] ?? []);
        $pageDocument->setLocale($locale);
        $pageDocument->setTitle($data['title']);
        $pageDocument->setResourceSegment($data['url']);
        $pageDocument->setStructureType($data['structureType'] ?? 'default');
        $pageDocument->setWorkflowStage(WorkflowStage::PUBLISHED);
        $pageDocument->getStructure()->bind($data);
        $pageDocument->setAuthor(1);
        $pageDocument->setExtensionsData($extensionData);

        if (isset($data['redirect'])) {
            $pageDocument->setRedirectType(RedirectType::EXTERNAL);
            $pageDocument->setRedirectExternal($data['redirect']);
        }

        $documentManager->persist(
            $pageDocument,
            $locale,
            ['parent_path' => $data['parent_path'] ?? '/cmf/beyouriq/contents']
        );

        // Set dataSource to current page after persist as uuid is before not available
        if (isset($data['pages']['dataSource']) && '__CURRENT__' === $data['pages']['dataSource']) {
            $pageDocument->getStructure()->bind(
                [
                    'pages' => array_merge(
                        $data['pages'],
                        [
                            'dataSource' => $pageDocument->getUuid(),
                        ]
                    ),
                ]
            );

            $documentManager->persist(
                $pageDocument,
                $locale,
                ['parent_path' => $data['parent_path'] ?? '/cmf/beyouriq/contents']
            );
        }

        $documentManager->publish($pageDocument, $locale);

        return $pageDocument;
    }

    private function getPathCleanup(): PathCleanup
    {
        if (null === $this->pathCleanup) {
            $this->pathCleanup = $this->container->get('sulu.content.path_cleaner');
        }

        return $this->pathCleanup;
    }

    private function getEntityManager(): EntityManagerInterface
    {
        if (null === $this->entityManager) {
            $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
        }

        return $this->entityManager;
    }
        /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return mixed[]
     * @throws MetadataNotFoundException
     *
     */
    private function loadPages(DocumentManager $documentManager): array
    {
        $pages = [
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Kampagnen',
                'name' => 'campaigns',
                'structureType' => 'campaign_selection',
                'url' => '/campaigns',
                'parent_path' => '/cmf/beyouriq/contents',
                'navigationContexts' =>  ['footer'],
                'seo' => [
                    'title' => 'Die Kampagnen',
                    'description' => 'Welche Kampagnen haben wir, planen wir und welche setzen wir gerade um',
                ],
            ],
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Artikel',
                'name' => 'articles',
                'structureType' => 'article_selection',
                'url' => '/articles',
                'parent_path' => '/cmf/beyouriq/contents',
                'navigationContexts' =>  ['footer'],
                'seo' => [
                    'title' => 'Die Artikel',
                    'description' => 'Darüber schreiben wir',
                ],
            ],
        ];

        foreach ($pages as $pageData) {
            $this->createPage($documentManager, $pageData);
        }

        $pages =  [
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Share Your News',
                'name' => 'share-your-news',
                'structureType' => 'campaign',
                'url' => '/campaings/share-your-news',
                'parent_path' => '/cmf/beyouriq/contents',
                'seo' => [
                    'title' => 'Share your News',
                    'description' => 'Wissenvermittlung und Challenge in einem.',
                ],
                'content' => '
                    <body id="page-top">

                    <!-- Navigation -->
                    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
                        <div class="container">
                        <a class="navbar-brand js-scroll-trigger" href="#page-top">Start Bootstrap</a>
                        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            Menu
                            <i class="fas fa-bars"></i>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#download">Download</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#features">Features</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                            </li>
                            </ul>
                        </div>
                        </div>
                    </nav>

                    <header class="masthead">
                        <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-lg-7 my-auto">
                            <div class="header-content mx-auto">
                                <h1 class="mb-5">New Age is an app landing page that will help you beautifully showcase your new mobile app, or anything else!</h1>
                                <a href="#download" class="btn btn-outline btn-xl js-scroll-trigger">Start Now for Free!</a>
                            </div>
                            </div>
                            <div class="col-lg-5 my-auto">
                            <div class="device-container">
                                <div class="device-mockup iphone6_plus portrait white">
                                <div class="device">
                                    <div class="screen">
                                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                    <img src="/campaigns/shareYourNews/img/demo-screen-1.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="button">
                                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </header>

                    <section class="download bg-primary text-center" id="download">
                        <div class="container">
                        <div class="row">
                            <div class="col-md-8 mx-auto">
                            <h2 class="section-heading">Discover what all the buzz is about!</h2>
                            <p>Our app is available on any mobile device! Download now to get started!</p>
                            <div class="badges">
                                <a class="badge-link" href="#"><img src="/campaigns/shareYourNews/img/google-play-badge.svg" alt=""></a>
                                <a class="badge-link" href="#"><img src="/campaigns/shareYourNews/img/app-store-badge.svg" alt=""></a>
                            </div>
                            </div>
                        </div>
                        </div>
                    </section>

                    <section class="features" id="features">
                        <div class="container">
                        <div class="section-heading text-center">
                            <h2>Unlimited Features, Unlimited Fun</h2>
                            <p class="text-muted">Check out what you can do with this app theme!</p>
                            <hr>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 my-auto">
                            <div class="device-container">
                                <div class="device-mockup iphone6_plus portrait white">
                                <div class="device">
                                    <div class="screen">
                                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                    <img src="/campaigns/shareYourNews/img/demo-screen-1.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="button">
                                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-lg-8 my-auto">
                            <div class="container-fluid">
                                <div class="row">
                                <div class="col-lg-6">
                                    <div class="feature-item">
                                    <i class="icon-screen-smartphone text-primary"></i>
                                    <h3>Device Mockups</h3>
                                    <p class="text-muted">Ready to use HTML/CSS device mockups, no Photoshop required!</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="feature-item">
                                    <i class="icon-camera text-primary"></i>
                                    <h3>Flexible Use</h3>
                                    <p class="text-muted">Put an image, video, animation, or anything else in the screen!</p>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6">
                                    <div class="feature-item">
                                    <i class="icon-present text-primary"></i>
                                    <h3>Free to Use</h3>
                                    <p class="text-muted">As always, this theme is free to download and use for any purpose!</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="feature-item">
                                    <i class="icon-lock-open text-primary"></i>
                                    <h3>Open Source</h3>
                                    <p class="text-muted">Since this theme is MIT licensed, you can use it commercially!</p>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </section>

                    <section class="cta">
                        <div class="cta-content">
                        <div class="container">
                            <h2>Stop waiting.<br>Start building.</h2>
                            <a href="#contact" class="btn btn-outline btn-xl js-scroll-trigger">Let\'s Get Started!</a>
                        </div>
                        </div>
                        <div class="overlay"></div>
                    </section>

                    <section class="contact bg-primary" id="contact">
                        <div class="container">
                        <h2>We
                            <i class="fas fa-heart"></i>
                            new friends!</h2>
                        <ul class="list-inline list-social">
                            <li class="list-inline-item social-twitter">
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                            </li>
                            <li class="list-inline-item social-facebook">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            </li>
                            <li class="list-inline-item social-google-plus">
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                            </li>
                        </ul>
                        </div>
                    </section>

                    <footer>
                        <div class="container">
                        <p>&copy; Your Website 2019. All Rights Reserved.</p>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                            <a href="#">Privacy</a>
                            </li>
                            <li class="list-inline-item">
                            <a href="#">Terms</a>
                            </li>
                            <li class="list-inline-item">
                            <a href="#">FAQ</a>
                            </li>
                        </ul>
                        </div>
                    </footer>

                    <!-- Bootstrap core JavaScript -->
                    <script src="/campaigns/shareYourNews//vendor/jquery/jquery.min.js"></script>
                    <script src="/campaigns/shareYourNews/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

                    <!-- Plugin JavaScript -->
                    <script src="/campaigns/shareYourNews/vendor/jquery-easing/jquery.easing.min.js"></script>

                    <!-- Custom scripts for this template -->
                    <script src="/campaigns/shareYourNews/js/new-age.js"></script>

                    </body>
                ',
                'styles' => '
                    <!-- Bootstrap core CSS -->
                    <link href="/campaigns/shareYourNews/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                
                    <!-- Custom fonts for this template -->
                    <link href="/campaigns/shareYourNews/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
                    <link rel="stylesheet" href="/campaigns/shareYourNews/vendor/simple-line-icons/css/simple-line-icons.css">
                    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
                    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
                    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
                
                    <!-- Plugin CSS -->
                    <link rel="stylesheet" href="/campaigns/shareYourNews/device-mockups/device-mockups.min.css">
                
                    <!-- Custom styles for this template -->
                    <link href="/campaigns/shareYourNews/css/new-age.min.css" rel="stylesheet">
                ',
            ],
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Impressum',
                'structureType' => 'default',
                'url' => '/impressum',
                'parent_path' => '/cmf/beyouriq/contents',
                'navigationContexts' =>  ['footer'],
                'seo' => [
                    'title' => 'Impressum',
                    'description' => 'Alles rund um das Rechtliche',
                ],
                'article' => '
                        <h3>Disclaimer:</h3>
                        <p>
                            Liability for contents
                            The contents of our pages were created with the greatest care. For the correctness, completeness and topicality of the contents we can take
                            over however no guarantee. As a service provider we are responsible according to § 7 Abs.1 TMG for our own contents on these pages according
                            to the general laws. According to §§ 8 to 10 TMG we are not obliged as a service provider to monitor transmitted or stored third-party information
                            or to investigate circumstances that indicate illegal activity. Obligations to remove or block the use of information in accordance with
                            general laws remain unaffected by this. However, liability in this respect is only possible from the time of knowledge of a concrete
                            violation of the law. As soon as we become aware of such infringements, we will remove the content immediately.
                        </p>
                        <h3>Liability for links</h3>
                        <p>
                            Our offer contains links to external websites of third parties, on whose contents we have no influence.
                            Therefore, we cannot assume any liability for these external contents. For the contents of the linked
                            The respective provider or operator of the pages is always responsible for the content of these pages. The linked pages
                            were checked for possible violations of the law at the time of linking. Illegal contents were
                            not recognizable at the time of linking. A permanent control of the contents of the linked pages is not possible.
                            pages is not reasonable without concrete evidence of an infringement. On becoming known
                            of law breakings we will remove such links immediately.
                        </p>
                        <h3>Copyright</h3>
                        <p>
                            The contents and works on these pages created by the site operators are subject to the copyright of their respective owners.
                            German copyright law. The duplication, treatment, spreading and each kind of the utilization
                            outside the limits of copyright law require the written consent of the respective author
                            or creator. Downloads and copies of this site are only for private, non-commercial use.
                            Use permitted. As far as the contents on this side were not provided by the operator, we are not liable for any postings or messages published by users of discussion boards, guestbooks or mailinglists provided on our page.
                            the copyrights of third parties. In particular, contents of third parties are marked as such.
                            Should you nevertheless become aware of an infringement of copyright, we ask you to send us an e-mail.
                            corresponding note. If we become aware of infringements of the law, we will not be held liable for such content.
                            immediately.
                        </p>
                        <h3>Data Policy</h3>
                        <p>
                            The use of our website is generally possible without providing personal data.
                            As far as on our pages personal data (such as name, address or e-mail addresses)
                            this is always done on a voluntary basis as far as possible. This data is collected without
                            Your express consent will not be disclosed to third parties.
                        </p>
                        <p>
                            We would like to point out that data transmission via the Internet (e.g. communication by e-mail) is not permitted without the express written consent of the respective party.
                            security vulnerabilities. A complete protection of the data from the access by third parties is
                            not possible.
                        </p>
                        <p>
                            The use of published postal addresses, telephone or fax numbers and email addresses for marketing purposes is prohibited, offenders sending unwanted spam messages will be punished.
                            The sending of unsolicited advertising and information material is hereby prohibited.
                            expressly contradicted. The operators of the pages expressly reserve the right to take legal action against the visitors of this website.
                            in the case of the unsolicited sending of advertising information, such as spam mails.
                        </p>
                        <p>
                            Developers For Future <br>
                            Responsible for web content: </br>
                            <small>
                            Maximilian Berghoff<br>
                            Birkenbachtal 26<br>
                            91617 Oberdachstetten<br>
                            E-Mail: <a href="mailto:info@beyouriq.org">info@beyouriq.org</a>
                            </small>
                        </p>
                '
            ],
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Datenschutz',
                'structureType' => 'default',
                'url' => '/datenschutz',
                'parent_path' => '/cmf/beyouriq/contents',
                'navigationContexts' =>  ['footer'],
                'seo' => [
                    'title' => 'Datenschutz',
                    'description' => 'Alles rund um den Datenschutz',
                ],
                'article' => '
                    <h3 id="dsg-general-intro"></h3>
                    <p>
                    This privacy policy clarifies the nature, scope and purpose of the processing of personal data
                    (hereinafter referred to as "data") within our online offer and the related websites, functions and contents as well as external online presences, e.g. our social
                    Media Profile (collectively referred to as the "Online Offering"). With regard to the terminology used, e.g. "Processing" or "Responsible"
                    We refer to the definitions in Article 4 of the General Data Protection Regulation (GDPR).<br>
                    </p>
                    <h3 id="dsg-general-controller">Responsible</h3>
                    <p>
                        Developers For Future <br>
                        <small>
                        Maximilian Berghoff<br>
                        Birkenbachtal 26<br>
                        91617 Oberdachstetten<br>
                        E-Mail: <a href="mailto:info@beyouriq.org">info@beyouriq.org</a>
                        </small>
                    </p>
                    <h3 id="dsg-general-datatype">Types of processed data:</h3>
                    <p>
                    - Usage data (e.g., websites visited, interest in content, access times).<br />
                    - Meta / communication data (e.g., device information, IP addresses).<br />
                    </p>
                    <h3 id="dsg-general-datasubjects">Kategorien betroffener Personen</h3>
                    <p>
                    Visitors and users of the online offer (hereinafter we refer to the persons concerned
                    in summary, as a "user").
                    </p>
                    <h3 id="dsg-general-purpose">Purpose of processing</h3>
                    <p>
                    - Providing the online offer, its features and content. <br />
                    - Security measures. <br />
                    - Reach Measurement / Marketing
                    </p>
                    <h3 id="dsg-general-terms">Used terms</h3>
                    <p>
                    "Personal information" is all information that relates to one
                    Identified or identifiable natural person (hereinafter referred to as "affected person"); a natural person is considered to be identifiable, directly or indirectly
                    indirectly, in particular by means of assignment to an identifier such as a name, to an identification number, to location data, to an online identifier (for example a
                    cookie) or to an or Several specific features can be identified, expressing the physical, physiological, genetic, mental, economic, cultural or social
                    Identity of this natural person are "Processing" means any process performed with or without the aid of automated procedures or any such process associated with personal data. Of the
                    Term goes far and includes virtually every deal with data.
                    <br />
                    "Pseudonymisation" means the processing of personal data in such a way that the personal data is no longer one without additional information
                    specific person may be assigned, provided that such additional information is kept separate and technical and organizational measures
                    ensuring that your personal information is not assigned to an identified or identifiable natural person.
                    <br />
                    "Profiling" means any kind of automated processing of personal data that consists in using that personal information to obtain certain personal information
                    To assess aspects relating to a natural person, in particular aspects of work performance, economic situation, health, personal preferences,
                    To analyze or predict interests, reliability, behavior, whereabouts or location of this natural person.
                    <br />
                    "Responsible" is the natural or legal person, authority, institution or other body, alone or in concert with others over the purposes and means
                    decides the processing of personal data. <br />
                    "Processor" means a natural or legal person, public authority, agency or other body providing personal data on behalf of the controller
                    processed.
                    </p>
                    <h3 id="dsg-general-legal-basis"> Relevant legal bases </h3>
                    <p>
                    In accordance with Art. 13 GDPR, we inform you about the legal basis of our data processing. Provided
                    The legal basis for obtaining consent is Article 6 (1) lit. a and Art. 7
                    DSGVO, the legal basis for the processing for the performance of our services and the execution of contractual measures as well as the answer to inquiries is Art. 6
                    para. 1 lit. b DSGVO, the legal basis for processing in order to fulfill our legal obligations is Art. 6 (1) lit. c DSGVO, and the legal basis for processing
                    To safeguard our legitimate interests, Art. 6 para. 1 lit. f DSGVO. In the event that vital interests of the person concerned or another natural
                    The person requiring the processing of personal data, Art. 6 (1) lit. d DSGVO as legal basis.
                    </p>
                    <h3 id="dsg-general-securitymeasures">Safety measures</h3>
                    <p>
                    In accordance with Art. 32 GDPR, we take into account the state of the art, the costs of implementation and the type and scope of the
                    Circumstances and purposes of the processing and the different likelihood and severity of the risk to the rights and freedoms of natural persons;
                    appropriate technical and organizational measures to ensure a level of protection commensurate with the risk. <br>
                    In particular, measures include ensuring the confidentiality, integrity and availability of data through the control of physical access to the data, as well as
                    access, input, disclosure, availability and separation. Furthermore, we have set up procedures that have a perception
                    of data subject rights, erasure of data and reaction to data threats. We also take into account the protection of personal data already at the
                    Development, or selection of hardware, software and procedures, according to the principle of data protection through technology design and privacy-friendly
                    Presettings (Article 25 DSGVO).
                    </p>
                    <h3 id="dsg-general-coprocessing">Collaboration with contract processors and third parties </h3>
                    <p>
                    Unless we provide data to other persons in the course of our processing and
                    To disclose, transmit to, or otherwise grant access to the data to any entity (processor or third party), this will only be done on the basis of a
                    statutory permission (for example, if a transmission of the data to third parties, such as payment service providers, pursuant to Article 6 (1) (b) GDPR is required to
                    fulfill the contract),
                    You have consented to a legal obligation or on the basis of our legitimate interests (such as the use of agents, web hosts, etc.).
                    <br />
                    If we commission third parties to process data on the basis of a so-called "contract processing contract", this is done on the basis of Art. 28 GDPR.
                    </p>
                    <h3 id="dsg-general-thirdparty"> Submissions to Third Countries </h3>
                    <p>
                    If we have data in a third country (ie outside the European Union (EU) or the European Union)
                    Economic Area (EEA)) or in the context of the use of third party services or disclosure, or transmission of data to third parties, this is done
                    only if it is to fulfill our (pre) contractual obligations, based on your consent, due to a legal obligation or based on our
                    legitimate interests happens. Subject to legal or contractual permissions, we process or disclose the data in a third country only if the
                    special requirements of Art. 44 et seq. DSGVO. That the processing is e.g. based on specific guarantees, such as the officially recognized finding
                    a level of data protection appropriate to the EU (eg for the USA through the "Privacy Shield") or observance of officially recognized special contractual obligations (see above) "standard contractual clauses").
                    </p>
                    <h3 id="dsg-general-rightssubject"> Rights of data subjects </h3>
                    <p>
                    You have the right to ask for confirmation whether relevant data are processed and for information about this data as well as for further information and a copy of the data according to Art. 15 GDPR.
                      <br />
                    You have accordingly. Art. 16 DSGVO the right to demand the completion of the data concerning you or the correction of the incorrect data concerning you.
                    <br />
                    In accordance with Art. 17 GDPR, they have the right to demand that the relevant data be deleted immediately or, alternatively, in accordance with Art. 18 GDPR
                    To require restriction of the processing of the data. <br />
                    You have the right to demand that the data relating to you, which you have provided to us, be obtained in accordance with Art. 20 GDPR and transmitted to others
                    Responsible ones to demand.
                    <br />
                    You have gem. Art. 77 DSGVO the right to file a complaint with the competent supervisory authority.
                    </p>
                    <h3 id="dsg-general-revokeconsent"> Right of Withdrawal </h3>
                    <p>
                    You have the right to grant consent in accordance with. Art. 7 para. 3 DSGVO with effect for the future
                    </p>
                    <h3 id="dsg-general-object"> right of objection </h3>
                    <p>
                    You can object to the future processing of your data in accordance with Art. 21 GDPR at any time. The opposition may be in particular against the processing
                    for direct marketing purposes.
                    </p>
                    <h3 id="dsg-general-cookies"> Cookies and Right to Oppose Direct Mail </h3>
                    <p>
                    "Cookies" are small files, which are stored on users\' computers. Different information can be stored within the cookies. A cookie is primarily used to provide information about one
                    To save users (or the device on which the cookie is stored) during or after their visit to an online offer. As temporary cookies, resp.
                    "Session cookies" or "transient cookies" are cookies that are deleted after a user leaves an online offer and closes his browser. In one
                    such cookie can e.g. the contents of a shopping cart in an online shop or a login status are saved. "Permanent" or "persistent" means cookies,
                    which remain saved even after closing the browser. Thus, e.g. the login status will be saved if users visit it after several days. As well
                    In such a cookie the interests of the users used for range measurement or marketing purposes can be stored. Become a third-party cookie
                    Cookies are offered by providers other than the person responsible for providing the online offer (otherwise, if only they are cookies)
                    one of "First-Party Cookies").
                    <br />
                    We can use temporary and permanent cookies and clarify this in the context of our privacy policy.
                    <br />
                    If users do not want cookies stored on their computer, they will be asked to select the appropriate option in the system settings of their browser
                    deactivate. Saved cookies can be deleted in the system settings of the browser. The exclusion of cookies may result in functional limitations of this
                    Online offer.
                    <br />
                    A general contradiction against the use of the cookies used for the purposes of online marketing can in a variety of services, especially in the case of tracking over
                    the US <a href="http://www.aboutads.info/choices/"> http://www.aboutads.info/choices/ </a> or the EU page <a href=" http://www.youronlinechoices.com/">http://www.youronlinechoices.com/ </a>
                    be explained. Furthermore, the storage of cookies can be achieved by switching them off in the settings of the browser. Please note that then
                    If necessary, not all functions of this online offer can be used.
                    </p>
                    <h3 id="dsg-general-erasure"> Deleting data</h3>
                    <p>
                    The data processed by us
                    are deleted or limited in their processing in accordance with Articles 17 and 18 GDPR. Unless explicitly stated in this Privacy Policy,
                    the data stored with us are deleted, as soon as they are no longer necessary for their purpose and the deletion no legal storage obligations
                    conflict. Unless the data is deleted because it is required for other and legitimate purposes, its processing will be restricted. That the
                    Data is blocked and not processed for other purposes. This applies, for example for data that needs to be kept for commercial or tax reasons.
                    <br />
                    According to legal requirements in Germany, the storage takes place in particular for 10 years according to §§ 147 Abs. 1 AO, 257 Abs. 1 Nr. 1 and 4, Abs. 4 HGB
                    (Bücher, Records, management reports, accounting documents, trading books, documents relevant to taxation, etc.) and 6 years pursuant to § 257 (1) nos. 2 and 3, para. 4 HGB (Business letters).
                    <br />
                    According to legal regulations in Austria the storage takes place in particular for 7 J according to § 132 Abs. 1 BAO (accounting documents, documents / invoices, accounts,vouchers,
                    Business papers, statement of income and expenses, etc.), for 22 years in connection with land and for 10 years in documents related to electronic
                    provided services, telecommunications, broadcasting and television services provided to non-entrepreneurs in EU Member States and for which the Mini-One-Stop-Shop (MOSS).
                    </p>
                    <h3 id="dsg-hostingprovider"> Hosting and Emailing </h3>
                    <p>
                    The hosting services we use are designed to provide the following services infrastructure and platform services, computing capacity, storage and database services, e-mail delivery, security and technical maintenance services, which we use for the purpose of operating this online offer.
                    <br />
                    Here we, or our hosting provider, process inventory data, contact data, content data, contract data, usage data, meta and communication data of customers, interested parties and visitors to this online offer on the basis of our legitimate interests in an efficient and secure provision of this online offer acc. Art. 6 para. 1 lit. f DSGVO i.V.m. Art. 28 GDPR (conclusion of contract processing contract).
                    </p>
                    <h3 id="dsg-logfiles"> Collection of Access Data and Logfiles </h3>
                    <p>
                    We, or our hosting provider, collects on the basis of our legitimate interests within the meaning of Art. 6 (1) lit. f. DSGVO Data on every access to the server on which this service is located (so-called server log files). The access data includes name of the retrieved web page, file, date and time of retrieval, amount of data transferred, message about successful retrieval, browser type and version, the user\'s operating system, referrer URL (the previously visited page), IP address and the requesting provider.
                    <br />
                    Logfile information is stored for security purposes (for example, to investigate abusive or fraudulent activities) for a maximum of 7 days and then deleted. Data whose further storage is required for evidential purposes are excluded from the deletion until final clarification of the respective incident. </span>
                    </p>
                    <h3 id="dsg-matomo">Range measurement with Matomo</h3>
                    <p>
                    As part of Matomo\'s range analysis, based on our legitimate interests (ie interest in the analysis, optimization and economic operation of our online offer as defined in Art. 6 para. 1 lit. f. DSGVO) processes the following data the type of browser you use and the browser version, the operating system you are using, your country of origin, the date and time of the server request, the number of visits, your time spent on the website and the external links you have activated. The IP address of the users is anonymized before being saved.
                    <br />
                    Matomo uses cookies that are stored on users\' computers and that allow an analysis of how users use our online offerings. In this case, pseudonymous usage profiles of the users can be created from the processed data. The cookies have a retention period of one week. The information generated by the cookie about your use of this website will only be stored on our server and will not be disclosed to third parties.
                    <br />
                    Users can object to the anonymized data collection by the program Matomo at any time with effect for the future by clicking on the link below. In this case, a so-called opt-out cookie is stored in your browser, with the result that Matomo no longer collects session data. However, if users delete their cookies, the result is that the opt-out cookie will also be deleted and therefore need to be re-enabled by users.
                    <br />
                    The logs containing the users\' data will be deleted after 6 months at the latest.
                    <br />
                    <iframe src="https://piwik.prberghoff.de/index.php?module=CoreAdminHome&action=optOut&language=en"></iframe>
                    </p>
                    <a href="https://datenschutz-generator.de" class="dsg1-5" rel="nofollow" target="_blank"> Created with Datenschutz-Generator.de by RA Dr. med. Thomas Schwenke </a>
                '
            ]
        ];
        foreach ($pages as $pageData) {
            $this->createPage($documentManager, $pageData);
        }

        return $pages;
    }

    /**
     * @throws DocumentManagerException
     */
    private function loadHomepage(DocumentManager $documentManager): void
    {
        /** @var HomeDocument $homeDocument */
        $homeDocument = $documentManager->find('/cmf/beyouriq/contents', AppFixtures::LOCALE_DE);

        $homeDocument->getStructure()->bind(
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => $homeDocument->getTitle(),
                'structureType' => 'homepage',
                'header_image' => null, #$this->getMediaId('KlimastreiNov_FB_post_1200x630 copy.jpg'),
                'parent_path' => '/cmf/beyouriq/contents',
                'url' => '/',
                'seo' => [
                    'title' => '',
                    'description' => '',
                    'description' => '',
                ],
                'header_title' => 'BeYourIQ',
                'header_sub_title' => 'green, sustainable, free',
                'service_title' => 'BeYourIQ',
                'service_description' => 'green, sustainable, free',
                'services' => [
                    [
                        'type' => 'service_item',
                        'service_title' => 'Service 1',
                        'service_description' => 'Beschreibung 1',
                        'service_icon' => 'envelope',
                    ],
                    [
                        'type' => 'service_item',
                        'service_title' => 'Service 2',
                        'service_description' => 'Beschreibung 2',
                        'service_icon' => 'envelope',
                    ],
                    [
                        'type' => 'service_item',
                        'service_title' => 'Service 3',
                        'service_description' => 'Beschreibung 3',
                        'service_icon' => 'envelope',
                    ],
                    [
                        'type' => 'service_item',
                        'service_title' => 'Service 4',
                        'service_description' => 'Beschreibung 4',
                        'service_icon' => 'envelope',
                    ],
                ],
                'vision_title' => '',
                'vision_description' => '',
                'visions' => [
                    [
                        'type' => 'vision',
                        'title' => 'Vision 1',
                        'description' => 'Beschreibung Vision 1'
                    ],
                    [
                        'type' => 'vision',
                        'title' => 'Vision 2',
                        'description' => 'Beschreibung Vision 3'
                    ],
                ],
                'members' => [
                    [
                        'type' => 'member',
                        'title' => 'Ann-Kathrin Berghoff',
                        'description' => 'Beschreibung Anni',
                    ],
                    [
                        'type' => 'member',
                        'title' => 'Maximilian Berghoff',
                        'description' => 'Beschreibung ',
                    ],
                ],
                'about_title_up' => 'A FEW WORDS',
                'about_title_down' => ' ABOUT US',
                'about_description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typek',
                'about_quote' => 'DevelopersForFuture',
            ]
        );

        $documentManager->persist($homeDocument, AppFixtures::LOCALE_DE);
    }

      /**
     * @throws DocumentManagerException
     * @throws MetadataNotFoundException
     * @throws Exception
     */
    public function load(DocumentManager $documentManager): void
    {
        $this->loadPages($documentManager);
        #$this->loadContactSnippet($documentManager);
        $this->loadHomepage($documentManager);

        // Needed, so that a Document use by loadHomepageGerman is managed.
        $documentManager->flush();
        #$this->updatePages($documentManager, AppFixtures::LOCALE_EN);

        $documentManager->flush();
    }
}
