var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
     // directory where compiled assets will be stored
     .setOutputPath('public/build/website/')
     // public path used by the web server to access the output path
     .setPublicPath('/build/website')
 
     .addEntry('app', './assets/js/app.js')
     .addEntry('base', './assets/scss/base.scss')
     // .addEntry('cookies', './assets/js/cookies.js')
     .disableSingleRuntimeChunk()
 
     .autoProvidejQuery()
 
     // allow sass/scss files to be processed
     .enableSassLoader()
     // allow legacy applications to use $/jQuery as a global variable
     .autoProvidejQuery()
     // empty the outputPath dir before each build
     .cleanupOutputBeforeBuild()
     // this is the default behavior...
     .enableSourceMaps(true)
     // show OS notifications when builds finish/fail
     .enableBuildNotifications()
;

module.exports = Encore.getWebpackConfig();
