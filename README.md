# beyouriq Website

## Prequisites

* make sure you have docker installed 
* share the drive where the cloned repository is in docker

**Windows**: 
Check if `git config core.autocrlf` is `false`
If it is not do: `$ git config --global core.autocrlf false`


## Usage Locally

Clone the repository by:

```bash
# if you have your public key in gitlab, you can choose the ssh way
git clone git@gitlab.com:beyouriq/website.git
# or
git clone https://gitlab.com/beyouriq/website.git
# jump into the folder

cd website
```

Copy the dist environment variables into the local env:

```bash
cp .env.dist .env
``

Having a lower docker-compose version forces you to read in the vars on
your own:

```bash
export $(egrep -v '^#' .env | xargs)
```

First switch to `master` branch, then read the current commit hash for the
docker image tag:

```bash
export CI_COMMIT_SHA=$(git rev-parse HEAD)
```

**Windows** you have to enter this command in the _Git Bash Command Line_

To run:

```bash
docker-compose -f docker-compose.development.yml up -d
```

When running it for the first time do:

```bash
docker-compose -f docker-compose.development.yml  exec sulu sh
# you are inside the docker container now, feels like a separate server
bin/console sulu:build dev
# Look good? (y) - Type y then enter/return
exit # to get out of it
```

(Hint: All of the dependencies are encapsulated inside a container,
you do not have to install anything locally)

Mow in your browser you should be able to visit:

* <http://0.0.0.0:8089> to see the homepage
* <http://0.0.0.0:8089/admin> to see the login of the admin panel
  (login: admin/admin for local usage)

**Windows**:
* <http://localhost:8089> to see the homepage
* <http://localhost:8089/admin> to see the login of the admin panel
  (login: admin/admin for local usage)
  
Read the docs for Sulu startup now:
<http://docs.sulu.io/en/2.0/book/getting-started.html>

To power down the container when you are finished developing, run:

```bash
docker-compose -f docker-compose.development.yml down
```
